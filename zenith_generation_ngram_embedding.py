import os
import yaml
import gpu_config
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM
from keras.optimizers import RMSprop
from keras.utils import multi_gpu_model
import tensorflow as tf
from tensorflow.python.client import device_lib
from keras.models import load_model
from zenith_data_generator_embedding import ZenithEmbeddingDataGenerator

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
sequence_length = config['input']['sequence_length']
ngram_size = config['embedding']['ngram_size']
output_vector_size = config['embedding']['output_vector_size']
lstm_layer_size = config['model']['lstm_layer_size']
dropout_rate = config['model']['dropout_rate']
learning_rate = config['model']['learning_rate']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
output_file_extension = config['output']['file_extension']
epochs = config['model']['epochs']
workers = config['generator']['workers']

num_gpus = len([x.name for x in device_lib.list_local_devices() if x.device_type == 'GPU'])

batch_size = config['model']['batch_size'] * num_gpus

ngram_input_dim = vocabulary_size ** ngram_size
ngram_input_length = (sequence_length // ngram_size) - 1  # Subtract one since we are predicting the next ngram


def define_model():
    if num_gpus > 1:
        with tf.device('/cpu:0'):
            model = Sequential()
            model.add(Embedding(input_dim=ngram_input_dim, output_dim=output_vector_size, input_length=ngram_input_length))
            model.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, return_sequences=True, implementation=2))
            if dropout_rate > 0.0:
                model.add(Dropout(dropout_rate))
            model.add(Dense(ngram_input_dim, activation='softmax'))
            model = multi_gpu_model(model)
            return model

    model = Sequential()
    model.add(Embedding(input_dim=ngram_input_dim, output_dim=output_vector_size, input_length=ngram_input_length))
    model.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, return_sequences=True, implementation=2))
    if dropout_rate > 0.0:
        model.add(Dropout(dropout_rate))
    model.add(Dense(ngram_input_dim, activation='softmax'))
    return model


model_load_filename = model_save_path + model_save_filename_prefix + output_file_extension

if os.path.isfile(model_load_filename):
    model = load_model(model_load_filename)
else:
    model = define_model()

print(model.summary())

optimizer = RMSprop(lr=learning_rate)

model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

model.fit_generator(ZenithEmbeddingDataGenerator(model, batch_size), epochs=epochs, use_multiprocessing=False, workers=workers, shuffle=False)
