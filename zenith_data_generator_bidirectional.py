import os
import yaml
import numpy as np
from keras.utils import Sequence
from keras.utils.np_utils import to_categorical
from datetime import datetime
from random import shuffle


class ZenithDataGeneratorBidirectional(Sequence):

    def __init__(self, model, batch_size):
        self.model = model
        self.batch_size = batch_size
        self.file_size = 0
        self.raw_sequences = []
        self.batch_buffer = {}

        with open("config.yml") as y:
            config = yaml.load(y)

        self.data_directory = config['input']['data_directory']
        self.input_file_extension = config['input']['extension']
        self.vocabulary_size = config['input']['vocabulary_size']
        self.sequence_length = config['input']['sequence_length']
        self.total_sequence_input_length = (self.sequence_length * 2) + 1
        self.step_size = config['sample']['step_size']
        self.model_save_path = config['output']['model_save_path']
        self.model_save_filename_prefix = config['output']['model_save_filename_prefix']
        self.output_file_extension = config['output']['file_extension']
        self.buffer_batches = config['generator']['buffer_batches']

        self.filenames = []
        for filename in os.listdir(self.data_directory):
            if filename.endswith(self.input_file_extension):
                self.filenames.append(filename)
                continue
            else:
                print(self.data_directory + '/' + filename + ' has an invalid extension.  Expecting ' + self.input_file_extension)
                continue

        self.next_filename_index = 0
        self.next_batch_index = 0
        self.__fill_buffer()

    def __len__(self):
        return int(np.ceil(self.file_size / float(self.batch_size)))

    def __getitem__(self, batch_index):
        batch_start = self.next_batch_index * self.batch_size
        batch_end = min(len(self.batch_buffer["one_hot_encoded_sequences_forward"]), (self.next_batch_index + 1) * self.batch_size)

        x = [np.array(self.batch_buffer["one_hot_encoded_sequences_forward"][batch_start:batch_end]), np.array(self.batch_buffer["one_hot_encoded_sequences_backward"][batch_start:batch_end])]
        y = np.array(self.batch_buffer["one_hot_encoded_outputs"][batch_start:batch_end])

        if self.next_batch_index + 1 == self.buffer_batches or batch_index + 1 == self.__len__():
            self.__fill_buffer()
        else:
            self.next_batch_index += 1

        return x, y

    def on_epoch_end(self):
        self.model.save(self.model_save_path + self.model_save_filename_prefix + '-' + datetime.now().strftime('%Y%m%d-%H%M%S') + self.output_file_extension)

    def __one_hot_encode_character(self, character):
        return to_categorical((ord(character) - 97), num_classes=self.vocabulary_size)

    def __one_hot_encode_sequence(self, sequence):
        sequence_of_int = [(ord(character) - 97) for character in sequence]

        return to_categorical(sequence_of_int, num_classes=self.vocabulary_size)

    def __fill_buffer(self):
        if len(self.raw_sequences) >= 0:
            self.__process_file(self.filenames[self.next_filename_index])

            if self.next_filename_index + 1 == len(self.filenames):
                self.next_filename_index = 0
            else:
                self.next_filename_index += 1

        self.next_batch_index = 0
        self.batch_buffer = {
            "one_hot_encoded_sequences_forward": [],
            "one_hot_encoded_sequences_backward": [],
            "one_hot_encoded_outputs": []
        }

        sequences_left = len(self.raw_sequences)
        for i in range(0, min(self.batch_size * self.buffer_batches, sequences_left)):
            sequence = self.raw_sequences.pop()

            self.batch_buffer["one_hot_encoded_sequences_forward"].append(self.__one_hot_encode_sequence(sequence[0:self.sequence_length]))
            self.batch_buffer["one_hot_encoded_sequences_backward"].append(self.__one_hot_encode_sequence(sequence[len(sequence) - self.sequence_length:len(sequence)]))
            self.batch_buffer["one_hot_encoded_outputs"].append(self.__one_hot_encode_character(sequence[len(sequence) - self.sequence_length - 1]))

    def __process_file(self, filename):
        self.file_size = 0
        self.raw_sequences = []

        with open(self.data_directory + '/' + filename) as file:
            for line in file:
                if not line:
                    continue

                # Take advantage of join/split to remove whitespace
                sequence = "".join(line.split()).lower().rstrip()
                for i in range(0, len(sequence), self.step_size):
                    if (i + self.total_sequence_input_length) > len(sequence):
                        break

                    self.raw_sequences.append(sequence[i:i + self.total_sequence_input_length])
                    self.file_size += 1

        shuffle(self.raw_sequences)
