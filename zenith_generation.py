import os
import yaml
import gpu_config
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM
from keras.optimizers import RMSprop
from keras.utils import multi_gpu_model
import tensorflow as tf
from tensorflow.python.client import device_lib
from keras.models import load_model
from zenith_data_generator import ZenithDataGenerator

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
sequence_length = config['input']['sequence_length'] - 1  # Subtract one since we are predict the next character
lstm_layer_size = config['model']['lstm_layer_size']
dropout_rate = config['model']['dropout_rate']
learning_rate = config['model']['learning_rate']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
output_file_extension = config['output']['file_extension']
epochs = config['model']['epochs']
workers = config['generator']['workers']

num_gpus = len([x.name for x in device_lib.list_local_devices() if x.device_type == 'GPU'])

batch_size = config['model']['batch_size'] * num_gpus


def define_model():
    if num_gpus > 1:
        with tf.device('/cpu:0'):
            model = Sequential()
            model.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, input_shape=(sequence_length, vocabulary_size), return_sequences=True, implementation=2))
            if dropout_rate > 0.0:
                model.add(Dropout(dropout_rate))
            model.add(Dense(vocabulary_size, activation='softmax'))
            model = multi_gpu_model(model)
            return model

    model = Sequential()
    model.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, input_shape=(sequence_length, vocabulary_size), return_sequences=True, implementation=2))
    if dropout_rate > 0.0:
        model.add(Dropout(dropout_rate))
    model.add(Dense(vocabulary_size, activation='softmax'))
    return model


model_load_filename = model_save_path + model_save_filename_prefix + output_file_extension

if os.path.isfile(model_load_filename):
    model = load_model(model_load_filename)
else:
    model = define_model()

print(model.summary())

optimizer = RMSprop(lr=learning_rate)

model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

model.fit_generator(ZenithDataGenerator(model, batch_size), epochs=epochs, use_multiprocessing=False, workers=workers, shuffle=False)