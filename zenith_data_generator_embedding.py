import os
import yaml
import numpy as np
from keras.utils import Sequence
from keras.utils.np_utils import to_categorical
from datetime import datetime
from random import shuffle


class ZenithEmbeddingDataGenerator(Sequence):

    def __init__(self, model, batch_size):
        self.model = model
        self.batch_size = batch_size
        self.file_size = 0
        self.raw_sequences = []
        self.batch_buffer = {}

        with open("config.yml") as y:
            config = yaml.load(y)

        self.data_directory = config['input']['data_directory']
        self.input_file_extension = config['input']['extension']
        self.vocabulary_size = config['input']['vocabulary_size']
        self.sequence_length = config['input']['sequence_length'] - 1  # Subtract one since we are predicting the next character
        self.model_save_path = config['output']['model_save_path']
        self.model_save_filename_prefix = config['output']['model_save_filename_prefix']
        self.output_file_extension = config['output']['file_extension']
        self.ngram_size = config['embedding']['ngram_size']
        self.buffer_batches = config['generator']['buffer_batches']

        self.ngram_input_dim = self.vocabulary_size ** self.ngram_size
        self.ngram_input_length = (self.sequence_length // self.ngram_size) - 1  # Subtract one since we are predicting the next ngram

        self.filenames = []
        for filename in os.listdir(self.data_directory):
            if filename.endswith(self.input_file_extension):
                self.filenames.append(filename)
                continue
            else:
                print(self.data_directory + '/' + filename + ' has an invalid extension.  Expecting ' + self.input_file_extension)
                continue

        self.next_filename_index = 0
        self.next_batch_index = 0
        self.__fill_buffer()

    def __len__(self):
        return int(np.ceil(self.file_size / float(self.batch_size)))

    def __getitem__(self, batch_index):
        batch_start = self.next_batch_index * self.batch_size
        batch_end = min(len(self.batch_buffer["integer_encoded_sequences"]), (self.next_batch_index + 1) * self.batch_size)

        x = np.array(self.batch_buffer["integer_encoded_sequences"][batch_start:batch_end])
        y = np.array(self.batch_buffer["one_hot_encoded_outputs"][batch_start:batch_end])

        if self.next_batch_index + 1 == self.buffer_batches or batch_index + 1 == self.__len__():
            self.__fill_buffer()
        else:
            self.next_batch_index += 1

        return x, y

    def on_epoch_end(self):
        self.model.save(self.model_save_path + self.model_save_filename_prefix + '-' + datetime.now().strftime('%Y%m%d-%H%M%S') + self.output_file_extension)

    def __one_hot_encode_sequence(self, sequence):
        return to_categorical(self.__integer_encode_sequence(sequence), num_classes=self.ngram_input_dim)

    def __integer_encode_sequence(self, sequence):
        sequence_of_int = []

        for x in range(0, len(sequence), self.ngram_size):
            if x + self.ngram_size > len(sequence):
                break

            next_ngram = sequence[x:(x + self.ngram_size)]

            unique_ngram_value = 0

            for i in range(0, len(next_ngram)):
                unique_ngram_value = unique_ngram_value + (ord(next_ngram[i]) - 97) * (26 ** i)

            sequence_of_int.append(unique_ngram_value)

        return sequence_of_int

    def __fill_buffer(self):
        if len(self.raw_sequences) == 0:
            self.__process_file(self.filenames[self.next_filename_index])

            if self.next_filename_index + 1 >= len(self.filenames):
                self.next_filename_index = 0
            else:
                self.next_filename_index += 1

        self.next_batch_index = 0
        self.batch_buffer = {
            "integer_encoded_sequences": [],
            "one_hot_encoded_outputs": []
        }

        sequences_left = len(self.raw_sequences)
        for i in range(0, min(self.batch_size * self.buffer_batches, sequences_left)):
            sequence = self.raw_sequences.pop()

            remainder = (len(sequence) % self.ngram_size)
            sequence = sequence[0:len(sequence) - remainder]
            self.batch_buffer["integer_encoded_sequences"].append(self.__integer_encode_sequence(sequence[0:self.ngram_size * -1]))
            self.batch_buffer["one_hot_encoded_outputs"].append(self.__one_hot_encode_sequence(sequence[self.ngram_size:]))

    def __process_file(self, filename):
        self.file_size = 0
        self.raw_sequences = []

        with open(self.data_directory + '/' + filename) as file:
            for line in file:
                if not line:
                    continue

                # Take advantage of join/split to remove whitespace
                sequence = "".join(line.split()).lower().rstrip()

                self.file_size += 1
                self.raw_sequences.append(sequence)

        shuffle(self.raw_sequences)
