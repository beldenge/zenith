import os
import yaml
import gpu_config
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, LSTM, concatenate
from keras.optimizers import RMSprop
from keras.utils import multi_gpu_model
import tensorflow as tf
from tensorflow.python.client import device_lib
from keras.models import load_model
from zenith_data_generator_bidirectional import ZenithDataGeneratorBidirectional

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
sequence_length = config['input']['sequence_length']
lstm_layer_size = config['model']['lstm_layer_size']
dropout_rate = config['model']['dropout_rate']
learning_rate = config['model']['learning_rate']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
output_file_extension = config['output']['file_extension']
epochs = config['model']['epochs']
workers = config['generator']['workers']

num_gpus = len([x.name for x in device_lib.list_local_devices() if x.device_type == 'GPU'])

batch_size = config['model']['batch_size'] * num_gpus


def define_model():
    with tf.device('/cpu:0'):
        model_forward = Sequential()
        model_forward.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, input_shape=(sequence_length, vocabulary_size), return_sequences=False, implementation=2))

        model_backward = Sequential()
        model_backward.add(LSTM(lstm_layer_size, dropout=dropout_rate, recurrent_dropout=dropout_rate, input_shape=(sequence_length, vocabulary_size), return_sequences=False, implementation=2, go_backwards=True))

        merged_output = concatenate([model_forward.output, model_backward.output])

        model_combined = Sequential()
        if dropout_rate > 0.0:
            model_combined.add(Dropout(dropout_rate))
        model_combined.add(Dense(vocabulary_size, activation='softmax'))

        final_model = Model([model_forward.input, model_backward.input], model_combined(merged_output))

        if num_gpus > 1:
            final_model = multi_gpu_model(final_model)

    return final_model


model_load_filename = model_save_path + model_save_filename_prefix + output_file_extension

if os.path.isfile(model_load_filename):
    model = load_model(model_load_filename)
else:
    model = define_model()

print(model.summary())

model.compile(loss='categorical_crossentropy', optimizer=RMSprop(lr=learning_rate), metrics=['accuracy'])

model.fit_generator(ZenithDataGeneratorBidirectional(model, batch_size), epochs=epochs, use_multiprocessing=False, workers=workers, shuffle=False)
