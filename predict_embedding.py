import os
import sys
import yaml
from decimal import *
import numpy as np
import gpu_config
import time
from keras.utils.np_utils import to_categorical
from keras.models import load_model
from flask import Flask
from flask import jsonify
from flask import request
app = Flask(__name__)

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
file_extension = config['output']['file_extension']
ngram_size = config['embedding']['ngram_size']
vocabulary_size = config['input']['vocabulary_size']

model_load_filename = model_save_path + model_save_filename_prefix + file_extension

if not os.path.isfile(model_load_filename):
    raise FileNotFoundError(model_load_filename)

model = load_model(model_load_filename)

ngram_input_dim = vocabulary_size ** ngram_size


def one_hot_encode_sequence(sequence):
    return to_categorical(integer_encode_sequence(sequence), num_classes=ngram_input_dim)


def integer_encode_sequence(sequence):
    sequence_of_int = []

    for x in range(0, len(sequence), ngram_size):
        if x + ngram_size > len(sequence):
            break

        next_ngram = sequence[x:(x + ngram_size)]

        unique_ngram_value = 0

        for i in range(0, len(next_ngram)):
            unique_ngram_value = unique_ngram_value + (ord(next_ngram[i]) - 97) * (26 ** i)

        sequence_of_int.append(unique_ngram_value)

    return sequence_of_int


# I have no idea why, but the method below does not work unless we make a prediction first
# ilikekillingpeoplebecauseitissomuchfunitismorefunthankillingwildgameintheforrestbecausemanisthemostdangeroueanamalofalltokillsomethinggivesmethemoatthrillingexperenceitisevenbetterthangettingyourrocksoffwithagirlthebestpartofitiathaewhenidieiwillbereborninparadicesndalltheihavekilledwillbecomemyslavesiwillnotgiveyoumynamebecauseyouwilltrytosloidownorstopmycollectingofslavesformyafterlifee
result = model.predict(np.array([integer_encode_sequence("ilikekillingpeoplebecauseitissomuchfunitismorefunthankillingwildgameintheforrestbecausemanisthemostdangeroueanamalofalltokillsomethinggivesmethemoatthrillingexperenceitisevenbetterthangettingyourrocksoffwithagirlthebestpartofitiathaewhenidieiwillbereborninparadicesndalltheihavekilledwillbecomemyslavesiwillnotgiveyoumynamebecauseyouwilltrytosloidownorstopmycollectingofslavesformyafterlife")]))

np.set_printoptions(threshold=sys.maxsize)


@app.route('/probabilities', methods=['POST'])
def probability():
    start = int(round(time.time() * 1000))

    request_json = request.get_json()

    probability_responses = []
    integer_encoded_sequences = []

    for sequence in request_json["sequences"]:
        integer_encoded_sequences.append(integer_encode_sequence(sequence[:ngram_size * -1]))

    probabilities = model.predict_on_batch(np.array(integer_encoded_sequences))

    for i in range(0, len(integer_encoded_sequences)):
        total_probability = Decimal('1.0')
        probability_list = []

        for j in range(0, (len(request_json["sequences"][i]) // ngram_size) - 1):  # Subtract one since we are predicting the next ngram
            one_hot_encoded_output = one_hot_encode_sequence(request_json["sequences"][i][(j * ngram_size) + ngram_size:(j * ngram_size) + (ngram_size * 2)])
            index_of_one = one_hot_encoded_output[0].nonzero()[0]

            actual_probability = probabilities[i][j][index_of_one]

            probability_list.append(actual_probability)
            total_probability = total_probability * Decimal(actual_probability.item())

        total_log_probability = np.sum(np.log(probability_list))

        probability_responses.append({"probability": str(total_probability), "logProbability": str(total_log_probability)})

    print("Request took " + str(int(round(time.time() * 1000)) - start) + "ms.")

    return jsonify({"probabilities": probability_responses})


