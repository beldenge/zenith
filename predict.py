import os
import sys
import yaml
from decimal import *
import numpy as np
import gpu_config
import time
from keras.utils.np_utils import to_categorical
from keras.models import load_model
from flask import Flask
from flask import jsonify
from flask import request
app = Flask(__name__)

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
file_extension = config['output']['file_extension']
step_size = config['sample']['step_size']
sequence_length = config['input']['sequence_length']

model_load_filename = model_save_path + model_save_filename_prefix + file_extension

if not os.path.isfile(model_load_filename):
    raise FileNotFoundError(model_load_filename)

model = load_model(model_load_filename)


def one_hot_encode_sequence(sequence):
    sequence = sequence.rstrip()
    sequence_of_int = [(ord(character) - 97) for character in sequence]

    return to_categorical(sequence_of_int, num_classes=vocabulary_size)


# I have no idea why, but the method below does not work unless we make a prediction first
# ilikekillingpeoplebecauseitissomuchfunitismorefunthankillingwildgameintheforrestbecausemanisthemostdangeroueanamalofalltokillsomethinggivesmethemoatthrillingexperenceitisevenbetterthangettingyourrocksoffwithagirlthebestpartofitiathaewhenidieiwillbereborninparadicesndalltheihavekilledwillbecomemyslavesiwillnotgiveyoumynamebecauseyouwilltrytosloidownorstopmycollectingofslavesformyafterlifee
result = model.predict(np.array([one_hot_encode_sequence("ilikekillingpeopleb")]))

np.set_printoptions(threshold=sys.maxsize)

@app.route('/probabilities', methods=['POST'])
def probability():
    start = int(round(time.time() * 1000))

    request_json = request.get_json()

    probability_responses = []
    slices_list = []
    one_hot_encoded_sequences = []

    for sequence in request_json["sequences"]:
        slices = []

        for i in range(0, len(sequence) - 1, step_size):
            if (i + sequence_length) > len(sequence):
                break

            next_slice = sequence[i:i + sequence_length]
            slices.append(next_slice)

            one_hot_encoded_sequences.append(one_hot_encode_sequence(next_slice[:-1]))

        slices_list.append(slices)

    probabilities = model.predict_on_batch(np.array(one_hot_encoded_sequences))

    for i in range(0, len(slices_list)):
        sequence_probabilities = probabilities[i * len(slices_list[0]):(i + 1) * len(slices_list[0])]
        sequence_slices = slices_list[i]
        total_probability = Decimal('1.0')
        probability_list = []

        for j in range(0, len(sequence_slices)):
            for k in range(0, len(sequence_slices[j]) - 1):
                character = sequence_slices[j][k + 1]
                actual_probability = sequence_probabilities[j][k][ord(character) - 97]
                probability_list.append(actual_probability)
                total_probability *= Decimal(actual_probability.item())

        total_log_probability = np.sum(np.log(probability_list))

        probability_responses.append({"probability": str(total_probability), "logProbability": str(total_log_probability)})

    print("Request took " + str(int(round(time.time() * 1000)) - start) + "ms.")

    return jsonify({"probabilities": probability_responses})


