import os
import yaml
import numpy as np
import gpu_config
import time
from keras.utils.np_utils import to_categorical
from keras.models import load_model

gpu_config.configure()

with open("config.yml") as y:
    config = yaml.load(y)

vocabulary_size = config['input']['vocabulary_size']
model_save_path = config['output']['model_save_path']
model_save_filename_prefix = config['output']['model_save_filename_prefix']
file_extension = config['output']['file_extension']

model_load_filename = model_save_path + model_save_filename_prefix + file_extension

if not os.path.isfile(model_load_filename):
    raise FileNotFoundError(model_load_filename)

model = load_model(model_load_filename)


def one_hot_encode_sequence(sequence):
    sequence = sequence.rstrip()
    sequence_of_int = [(ord(character) - 97) for character in sequence]

    return to_categorical(sequence_of_int, num_classes=vocabulary_size)


seed = "ilikekillingpeoplebecauseitissomuchfunit"
generated_text = seed

temperature = 0.5
generated_length = 50

for i in range(generated_length):
    predictions = model.predict(np.array([one_hot_encode_sequence(seed)]))[0]
    predictions = np.asarray(predictions).astype('float64') # I'm not sure this line is really needed
    predictions = np.log(predictions) / temperature
    exp_predictions = np.exp(predictions)
    predictions = exp_predictions / np.sum(exp_predictions)
    probabilities = np.random.multinomial(1, predictions)
    chosen = np.argmax(probabilities)
    generated_text = generated_text + chr(chosen + 97)
    seed = seed[1:] + chr(chosen + 97)

print("generated: " + generated_text)
