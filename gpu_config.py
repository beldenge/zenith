import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


def configure():
    tfconfig = tf.ConfigProto()
    tfconfig.gpu_options.allow_growth = True
    tfconfig.gpu_options.per_process_gpu_memory_fraction = 0.95
    tfconfig.allow_soft_placement = True
    sess = tf.Session(config=tfconfig)
    set_session(sess)
